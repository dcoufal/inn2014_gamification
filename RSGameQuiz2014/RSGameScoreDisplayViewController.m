//
//  RSGameScoreDisplayViewController.m
//  RSGameQuiz2014
//
//  Created by David Coufal on 12/1/14.
//  Copyright (c) 2014 rosettastone. All rights reserved.
//

#import "RSGameScoreDisplayViewController.h"
#import "UIColor+RSGame.h"
#import "UIView+AutoLayout.h"
#import "RSGameConfettiView.h"

@interface RSGameScoreDisplayViewController ()

@property (weak, nonatomic) IBOutlet UILabel *yourscorewasLabel;
@property (weak, nonatomic) IBOutlet UIButton *dismissButton;
@property (weak, nonatomic) IBOutlet UIView *confettiContainerView;
@property (weak, nonatomic) IBOutlet UILabel *shareYourScoreLabel;
@property (weak, nonatomic) IBOutlet UIImageView *facebookImageView;
@property (weak, nonatomic) IBOutlet UIImageView *googleplusImageView;
@property (weak, nonatomic) IBOutlet UIImageView *twitterImageView;

@end

@implementation RSGameScoreDisplayViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES
                                            withAnimation:UIStatusBarAnimationFade];
    
    self.view.backgroundColor = [UIColor rs_gameBackgroundColor];

    self.scoreDisplayLabel.format = @"%d";
    self.scoreDisplayLabel.text = @"0";
    
    [self.view configureView: self.scoreDisplayLabel];
    self.scoreDisplayLabel.textColor = [UIColor whiteColor];

    self.yourscorewasLabel.textColor = [UIColor whiteColor];
    self.shareYourScoreLabel.textColor = [UIColor whiteColor];
    
    [self.view configureView: self.dismissButton];
    [self.dismissButton setTitleColor:[UIColor rs_rosettaYellowColor] forState:UIControlStateNormal];
    self.dismissButton.layer.borderColor = [UIColor rs_rosettaYellowColor].CGColor;
    self.dismissButton.backgroundColor = [UIColor rs_gameHighlightColor];
    
    self.facebookImageView.image = [self.facebookImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.googleplusImageView.image = [self.googleplusImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.twitterImageView.image = [self.twitterImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.3 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
        
        static const CGFloat duration = 0.5;
        
        [self.scoreDisplayLabel countFromCurrentValueTo:self.score withDuration:duration];
        
        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, duration * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
            RSGameConfettiView *view = [[RSGameConfettiView alloc] initWithFrame:self.confettiContainerView.frame];
            view.translatesAutoresizingMaskIntoConstraints = NO;
            
            [self.confettiContainerView addSubview:view];
            [self.confettiContainerView rs_addEqualContentConstraintsToAllEdgesOfSubview:view];
            
            static const CGFloat spinUpDuration = 2.0;
            
            [view spinUpAndDecayOver: spinUpDuration];
        });
    });
}

- (IBAction)dismissTapped:(UIButton *)sender {
    [self.presentingViewController.presentingViewController dismissViewControllerAnimated:NO completion:nil];
//    [self performSegueWithIdentifier:@"returnToStart" sender:self];
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

@end
