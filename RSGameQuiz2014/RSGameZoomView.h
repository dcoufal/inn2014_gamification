//
//  RSGameZoomView.h
//  RSGameQuiz2014
//
//  Created by David Coufal on 12/2/14.
//  Copyright (c) 2014 rosettastone. All rights reserved.
//

#import <UIKit/UIKit.h>

extern const CGFloat RSGameZoomViewDimension;

@interface RSGameZoomView : UIView

-(void)configureForSuccess;
-(void)configureForFailure;
-(void)configureForGameStart;

@end
