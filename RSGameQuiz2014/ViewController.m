//
//  ViewController.m
//  RSGameQuiz2014
//
//  Created by David Coufal on 12/1/14.
//  Copyright (c) 2014 rosettastone. All rights reserved.
//

#import "ViewController.h"
#import "UIColor+RSGame.h"
#import "UIView+AutoLayout.h"
#import "RSStartVCTransitioning.h"

#define DEGREES_TO_RADIANS(x) (M_PI * (x) / 180.0)


@interface ViewController ()

@property (weak, nonatomic) IBOutlet UIImageView *rsLogoImageView;
@property (weak, nonatomic) IBOutlet UIImageView *rsStoneImageView;
@property (weak, nonatomic) IBOutlet UIImageView *rsNameImageView;
@property (weak, nonatomic) IBOutlet UIImageView *mountainImageView;
@property (weak, nonatomic) IBOutlet UIImageView *treeline2ImageView;
@property (weak, nonatomic) IBOutlet UIImageView *treeline3ImageView;
@property (weak, nonatomic) IBOutlet UIImageView *treeline4ImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *planetImageTop;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingBackground1;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingBackground2;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingBackground3;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *leadingBackground4;
@property (weak, nonatomic) IBOutlet UIButton *continueButton;
@property (weak, nonatomic) IBOutlet UIImageView *starfieldImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *continueToBottomConstraint;
@property (weak, nonatomic) IBOutlet UIButton *refreshButton;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *refreshToSideConstraint;

// shooting star
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ss1Horiz;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ss1Vert;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ss2horiz;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ss2vert;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ss3horiz;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *ss3vert;
@property (weak, nonatomic) IBOutlet UIImageView *ss1ImageView;
@property (weak, nonatomic) IBOutlet UIImageView *ss2ImageView;
@property (weak, nonatomic) IBOutlet UIImageView *ss3ImageView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES
                                            withAnimation:UIStatusBarAnimationFade];
    
    self.view.backgroundColor = [UIColor blackColor];
    
    self.rsLogoImageView.tintColor = [UIColor blackColor];
    self.rsStoneImageView.tintColor = [UIColor rs_rosettaBlueColor];
    self.rsNameImageView.tintColor = [UIColor rs_rosettaYellowColor];
    self.mountainImageView.tintColor = [UIColor rs_gameBackgroundColor];
    self.treeline2ImageView.tintColor = [UIColor colorWithRed:0.055 green:0.18 blue:0.184 alpha:1.0];
    self.treeline3ImageView.tintColor = [UIColor colorWithRed:0.07 green:0.24 blue:0.25 alpha:1.0];
    self.treeline4ImageView.tintColor = [UIColor colorWithRed:0.094 green:0.286 blue:0.294 alpha:1.0];
    self.ss1ImageView.tintColor = [UIColor whiteColor];
    self.ss2ImageView.tintColor = [UIColor whiteColor];
    self.ss3ImageView.tintColor = [UIColor whiteColor];
    
    self.rsLogoImageView.image = [self.rsLogoImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.rsStoneImageView.image = [self.rsStoneImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.rsNameImageView.image = [self.rsNameImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.mountainImageView.image = [self.mountainImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.treeline2ImageView.image = [self.treeline2ImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.treeline3ImageView.image = [self.treeline3ImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.treeline4ImageView.image = [self.treeline4ImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.ss1ImageView.image = [self.treeline4ImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.ss2ImageView.image = [self.treeline4ImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.ss3ImageView.image = [self.treeline4ImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    UIImage *blackRefreshImage = [UIImage imageNamed:@"refresh"];
    UIImage *yellowRefreshImage = [blackRefreshImage imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    [self.refreshButton setImage:yellowRefreshImage forState:UIControlStateNormal];
    self.refreshButton.tintColor = [UIColor rs_rosettaYellowColor];
    
    [self.view configureView: self.continueButton];
    [self.continueButton setTitleColor:[UIColor rs_rosettaYellowColor] forState:UIControlStateNormal];
    self.continueButton.layer.borderColor = [UIColor rs_rosettaYellowColor].CGColor;
    self.continueButton.backgroundColor = [UIColor rs_gameHighlightColor];
    
    [self.view configureView: self.refreshButton];
    self.refreshButton.layer.borderColor = [UIColor rs_rosettaYellowColor].CGColor;
    self.refreshButton.backgroundColor = [UIColor rs_gameHighlightColor];
    
    [self setAnimationToStart:NO];
}

-(void)setAnimationToStart:(BOOL)animated
{
    void (^setup)() = ^void(){
        self.ss2horiz.constant = 20.0f;
        self.ss3horiz.constant = 47.0f;
        
        self.ss1Horiz.constant = 450.0;
        self.ss1Vert.constant = -70.0;
        self.ss2vert.constant = -110.0;
        self.ss3vert.constant = -60.0;
        
        self.ss1ImageView.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(60.0));
        self.ss2ImageView.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(60.0));
        self.ss3ImageView.transform = CGAffineTransformMakeRotation(DEGREES_TO_RADIANS(60.0));
        
        self.rsStoneImageView.alpha = 0.0;
        self.rsNameImageView.alpha = 0.0;
        self.rsLogoImageView.alpha = 1.0;
        
        self.starfieldImageView.alpha = 0.0;
        
        self.continueButton.hidden = YES;
        self.continueButton.alpha = 0.0;
        
        self.refreshButton.hidden = YES;
        self.refreshButton.alpha = 0.0;
        
        self.leadingBackground2.constant = 0.0;
        self.leadingBackground3.constant = 0.0f;
        self.leadingBackground4.constant = 0.0f;
        self.planetImageTop.constant = 300.0f;
    };
    
    if (animated) {
        [UIView animateWithDuration:0.4
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             setup();
                             [self.view layoutIfNeeded];
                         }
                         completion:^(BOOL finished){
                             dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.4 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                                 [self runAnimations];
                             });
                         }];
    }
    else {
        setup();
        [self.view layoutIfNeeded];
    }
}

-(void)runAnimations
{
    [UIView animateWithDuration:2.0
                          delay:5.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.starfieldImageView.alpha = 1.0;
                     }
                     completion:nil];
    
    [UIView animateWithDuration:7.0
                          delay:0.5
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.leadingBackground2.constant = -80.0f;
                         self.leadingBackground3.constant = -160.0f;
                         self.leadingBackground4.constant = -320.0f;
                         self.planetImageTop.constant = 40.0f;
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL finished){
                         
                         self.continueButton.hidden = NO;
                         self.refreshButton.hidden = NO;
                         [UIView animateWithDuration:1.0
                                               delay:0.5
                                             options:UIViewAnimationOptionCurveEaseInOut
                                          animations:^{
                                              self.continueButton.alpha = 1.0;
                                              self.refreshButton.alpha = 1.0;
                                          }
                                          completion:nil];
                         
                         [UIView animateWithDuration:1.0
                                               delay:0.0
                                             options:UIViewAnimationOptionCurveEaseInOut
                                          animations:^{
                                              self.rsStoneImageView.alpha = 1.0;
                                              self.rsNameImageView.alpha = 1.0;
                                              self.rsLogoImageView.alpha = 0.0;
                                          }
                                          completion:nil];
                         
                         [UIView animateWithDuration:1.0
                                               delay:1.1
                                             options:UIViewAnimationOptionCurveEaseInOut
                                          animations:^{
                                              self.ss1Horiz.constant = -150.0;
                                              self.ss1Vert.constant = 230.0;
                                              self.ss2vert.constant = 190.0;
                                              self.ss3vert.constant = 240.0;
                                              [self.view layoutIfNeeded];
                                          }
                                          completion:nil];
                     }];
}

-(void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    [self runAnimations];
}


- (IBAction)continueButtontapped:(UIButton *)sender {
    [UIView animateWithDuration:0.3
                          delay:0.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.continueToBottomConstraint.constant = -80.0;
                         self.refreshToSideConstraint.constant = -80.0;
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}

- (IBAction)refreshAnimationButton:(UIButton *)sender {
    [self setAnimationToStart:YES];
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    ((UIViewController *)segue.destinationViewController).modalPresentationStyle = UIModalPresentationCustom;
    ((UIViewController *)segue.destinationViewController).transitioningDelegate = self;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                  presentingController:(UIViewController *)presenting
                                                                      sourceController:(UIViewController *)source {
    
    RSStartVCTransitioning *animator = [RSStartVCTransitioning new];
    animator.presenting = YES;
    return animator;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    RSStartVCTransitioning *animator = [RSStartVCTransitioning new];
    animator.presenting = NO;
    return animator;
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

@end
