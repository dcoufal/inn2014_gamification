//
//  RSGameVCTransitioning.m
//  RSGameQuiz2014
//
//  Created by David Coufal on 12/2/14.
//  Copyright (c) 2014 rosettastone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSGameVCTransitioning.h"
#import "RSGameZoomView.h"
#import "UIView+AutoLayout.h"
#import "UIColor+RSGame.h"
#import "RSGameViewController.h"

@implementation RSGameVCTransitioning

- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext {
    return 0.5f;
}

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext {
    // Grab the from and to view controllers from the context
    UIViewController *fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController *toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    if (self.presenting) {
        fromViewController.view.userInteractionEnabled = NO;
        
        // Transitioning to this view, place in back
        [transitionContext.containerView addSubview:toViewController.view];
        
        // take a screenshot of our current VC and display in Image View
        UIGraphicsBeginImageContextWithOptions(fromViewController.view.bounds.size, NO, [UIScreen mainScreen].scale);
        
        [fromViewController.view drawViewHierarchyInRect:fromViewController.view.bounds afterScreenUpdates:YES];
        
        UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
        imageView.translatesAutoresizingMaskIntoConstraints = NO;
        [transitionContext.containerView addSubview:imageView];
        [transitionContext.containerView rs_addEqualContentConstraintsToAllEdgesOfSubview:imageView];
        
        // create a background view with just a field of color to cover up the buttons from the previous screen.
        UIView *backgroundView = [UIView new];
        backgroundView.backgroundColor = [UIColor rs_gameBackgroundColor];
        backgroundView.translatesAutoresizingMaskIntoConstraints = NO;
        [transitionContext.containerView addSubview:backgroundView];
        [transitionContext.containerView rs_addEqualContentConstraintsToAllEdgesOfSubview:backgroundView];
        backgroundView.alpha = 0.0;
       
        // create anther background with a hole in the middle. We animate later to make the hole bigger
        UIView *holeBackgroundView = [UIView new];
        holeBackgroundView.backgroundColor = [UIColor clearColor];
        holeBackgroundView.translatesAutoresizingMaskIntoConstraints = NO;
        [transitionContext.containerView addSubview:holeBackgroundView];
        [transitionContext.containerView rs_addCenterConstraintsToSubview:holeBackgroundView];
        [holeBackgroundView rs_addHeightConstraint:CGRectGetHeight(fromViewController.view.bounds)];
        [holeBackgroundView rs_addWidthConstraint:CGRectGetWidth(fromViewController.view.bounds)];
        
        int radius = RSGameZoomViewDimension * 0.45 * 0.5;
        UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0,
                                                                                0,
                                                                                CGRectGetWidth(fromViewController.view.bounds),
                                                                                CGRectGetHeight(fromViewController.view.bounds))
                                                        cornerRadius:0];
        UIBezierPath *circlePath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(CGRectGetWidth(fromViewController.view.bounds) / 2.0 - radius,
                                                                                      CGRectGetHeight(fromViewController.view.bounds) / 2.0 - radius,
                                                                                      2.0*radius,
                                                                                      2.0*radius)
                                                              cornerRadius:radius];
        [path appendPath:circlePath];
        [path setUsesEvenOddFillRule:YES];
        
        CAShapeLayer *fillLayer = [CAShapeLayer layer];
        fillLayer.path = path.CGPath;
        fillLayer.fillRule = kCAFillRuleEvenOdd;
        fillLayer.fillColor = [UIColor rs_gameBackgroundColor].CGColor;
        fillLayer.opacity = 1.0;
        [holeBackgroundView.layer addSublayer:fillLayer];
        
        holeBackgroundView.alpha = 0.0;
        
        // a view with some fun text on it to introduce the quiz
        RSGameZoomView * zoomView = [RSGameZoomView new];
        zoomView.transform = CGAffineTransformMakeScale(0.1, 0.1);
        zoomView.alpha = 0.0f;
        [transitionContext.containerView addSubview:zoomView];
        [transitionContext.containerView rs_addCenterConstraintsToSubview:zoomView];
        
        [zoomView configureForGameStart];
        
        [transitionContext.containerView layoutIfNeeded];
        
        [UIView animateWithDuration:0.5
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             backgroundView.alpha = 1.0;
                         }
                         completion:nil];

        
        [UIView animateWithDuration:0.6
                              delay:0.0
             usingSpringWithDamping:0.4
              initialSpringVelocity:1.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             zoomView.transform = CGAffineTransformRotate( CGAffineTransformMakeScale(0.5, 0.5), .2);
                             zoomView.alpha = 1.0;
                             [transitionContext.containerView layoutIfNeeded];
                         }
                         completion:^(BOOL finished){
                             imageView.alpha = 0.0;
                             holeBackgroundView.alpha = 1.0;
                             backgroundView.alpha = 0.0;
                             [backgroundView removeFromSuperview];
                             
                             [transitionContext.containerView layoutIfNeeded];
                             
                             [UIView animateWithDuration:0.7
                                                   delay:1.0
                                                 options:UIViewAnimationOptionCurveEaseInOut
                                              animations:^{
                                                  zoomView.transform = CGAffineTransformRotate( CGAffineTransformMakeScale(1.0, 1.0), -1.2);
                                                  zoomView.alpha = 0.0;
                                                  holeBackgroundView.transform = CGAffineTransformMakeScale(10.0, 10.0);
                                                  
                                                  [transitionContext.containerView layoutIfNeeded];
                                              }
                                              completion:^(BOOL finished){
                                                  RSGameViewController * gameVC = (RSGameViewController *)toViewController;
                                                  [gameVC startQuiz];
                                                  
                                                  fromViewController.view.userInteractionEnabled = YES;
                                                  
                                                  [zoomView removeFromSuperview];
                                                  [imageView removeFromSuperview];
                                                  [holeBackgroundView removeFromSuperview];
                                                  
                                                  [transitionContext completeTransition:YES];
                                              }];
                             
                         }];
    }
    else {
        [transitionContext.containerView addSubview:fromViewController.view];
        [transitionContext.containerView addSubview:toViewController.view];
        toViewController.view.alpha = 0.0;
        toViewController.view.transform = CGAffineTransformMakeScale(10.0, 10.0);
        
        [UIView animateWithDuration:0.6
                              delay:0.0
                            options:UIViewAnimationOptionCurveEaseInOut
                         animations:^{
                             fromViewController.view.alpha = 0.0;
                             fromViewController.view.transform = CGAffineTransformMakeScale(0.1, 0.1);
                             toViewController.view.alpha = 1.0;
                             toViewController.view.transform = CGAffineTransformMakeScale(1.0, 1.0);
                             [transitionContext.containerView layoutIfNeeded];
                         }
                         completion:^(BOOL finished){
                             fromViewController.view.alpha = 1.0;
                             fromViewController.view.transform = CGAffineTransformIdentity;
                             [transitionContext completeTransition:YES];
                         }];
    }
    
}

@end
