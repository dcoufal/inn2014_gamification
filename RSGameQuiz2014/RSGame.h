//
//  RSGame.h
//  RSGameQuiz2014
//
//  Created by David Coufal on 12/1/14.
//  Copyright (c) 2014 rosettastone. All rights reserved.
//

#import <Foundation/Foundation.h>

typedef NS_ENUM(NSUInteger, RSGameMultipier) {
    RSGameMultipier1X,
    RSGameMultipier2X,
    RSGameMultipier3X,
    RSGameMultipier4X
};
typedef NS_ENUM(NSUInteger, RSGameCountdownStatus) {
    RSGameCountdownStatusGreat,
    RSGameCountdownStatusFaster,
    RSGameCountdownStatusWeak,
};

@interface RSGame : NSObject

@property (readonly, nonatomic) RSGameMultipier multipler;
@property (readonly, nonatomic) NSString *multiplerText;

@property (readonly, nonatomic) NSUInteger score;
@property (readonly, nonatomic) NSString *scoreText;

@property (readonly, nonatomic) NSString *questionText;
@property (readonly, nonatomic) NSString *answer1Text;
@property (readonly, nonatomic) NSString *answer2Text;
@property (readonly, nonatomic) NSString *answer3Text;
@property (readonly, nonatomic) NSString *answer4Text;

@property (readonly, nonatomic) BOOL endOfQuestions;

-(instancetype)initWithNumberOfQuestions:(NSUInteger)numberOfQuestions;
-(void)startQuestion:(void(^)(float countdownValue, RSGameCountdownStatus countdownStatus))callback;
-(BOOL)answer:(NSUInteger)answerNumber;

@end
