//
//  SadnessBurstView.m
//  RSGameQuiz2014
//
//  Created by David Coufal on 12/3/14.
//  Copyright (c) 2014 rosettastone. All rights reserved.
//

#import "SadnessBurstView.h"
#import "UIColor+RSGame.h"

@interface SadnessBurstView ()

@property (nonatomic, copy) void (^completionCallback)(void);

@end

@implementation SadnessBurstView {
    __weak CAEmitterLayer *_emitter;
}


-(id)initWithCompletion:(void(^)(void))completion frame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        _completionCallback = completion;
        
        self.userInteractionEnabled = NO;
        self.backgroundColor = [UIColor clearColor];
        
        CAEmitterLayer *emitterLayer = (CAEmitterLayer*)self.layer;
        
        emitterLayer.name = @"sadnessemitterLayer";
        emitterLayer.emitterPosition = CGPointMake(self.bounds.size.width /2, self.bounds.size.height /2);
        emitterLayer.emitterZPosition = 0;
        emitterLayer.emitterShape = kCAEmitterLayerPoint;
        emitterLayer.emitterMode = kCAEmitterLayerVolume;
        
        emitterLayer.emitterSize = CGSizeMake(2.00, 2.00);
        emitterLayer.emitterDepth = 0.00;
        
        emitterLayer.renderMode = kCAEmitterLayerAdditive;
        
        emitterLayer.seed = arc4random() % 600000;
        
        
        
        
        // Create the emitter Cell
        CAEmitterCell *emitterCell = [CAEmitterCell emitterCell];
        
        emitterCell.name = @"success";
        emitterCell.enabled = YES;
        
        emitterCell.contents = (id)[[UIImage imageNamed:@"sadness"] CGImage];
        emitterCell.contentsRect = CGRectMake(0.00, 0.00, 1.00, 1.00);
        
        emitterCell.magnificationFilter = kCAFilterLinear;
        emitterCell.minificationFilter = kCAFilterLinear;
        emitterCell.minificationFilterBias = 0.00;
        
        emitterCell.scale = 0.6;
        emitterCell.scaleRange = 0.00;
        emitterCell.scaleSpeed = 0.0;
        
        emitterCell.color = [[UIColor rs_incorrectRed] CGColor];
        emitterCell.redRange = 0.00;
        emitterCell.greenRange = 0.00;
        emitterCell.blueRange = 0.00;
        emitterCell.alphaRange = 0.00;
        
        emitterCell.redSpeed = 0.00;
        emitterCell.greenSpeed = 0.00;
        emitterCell.blueSpeed = 0.00;
        emitterCell.alphaSpeed = -0.50;
        
        emitterCell.lifetime = 1.0;
        emitterCell.lifetimeRange = 0.0;
        emitterCell.birthRate = 25;
        emitterCell.velocity = 175.0;
        emitterCell.velocityRange = 0.00;
        emitterCell.xAcceleration = 0.00;
        emitterCell.yAcceleration = 700.00;
        emitterCell.zAcceleration = 0;
        
        // these values are in radians, in the UI they are in degrees
        emitterCell.spin = 0.000;
        emitterCell.spinRange = 0;
        emitterCell.emissionLatitude = 0.000;
        emitterCell.emissionRange = (CGFloat) M_PI;
        emitterCell.emissionLongitude = (CGFloat) M_PI;
        
        
        
        emitterLayer.emitterCells = @[emitterCell];
        
        _emitter = emitterLayer;
        
        [self performSelector:@selector(stopEmitting) withObject:nil afterDelay:0.1];
    }
    
    return self;
}

- (void) stopEmitting {
    _emitter.birthRate = 0.0;
    if (self.completionCallback) {
        self.completionCallback();
    }
}

+ (Class) layerClass {
    return [CAEmitterLayer class];
}

@end
