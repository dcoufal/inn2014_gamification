//
//  RSGameViewController.h
//  RSGameQuiz2014
//
//  Created by David Coufal on 12/1/14.
//  Copyright (c) 2014 rosettastone. All rights reserved.
//

#import <UIKit/UIKit.h>

@class RSGame;

@interface RSGameViewController : UIViewController<UIViewControllerTransitioningDelegate>

@property (strong, nonatomic) RSGame *game;

-(void)startQuiz;

@end
