//
//  RSGameZoomView.m
//  RSGameQuiz2014
//
//  Created by David Coufal on 12/2/14.
//  Copyright (c) 2014 rosettastone. All rights reserved.
//

#import "RSGameZoomView.h"
#import "UIView+AutoLayout.h"
#import "UIColor+RSGame.h"

const CGFloat RSGameZoomViewDimension = 300.0f;

@interface RSGameZoomView ()

@property (weak, nonatomic) UILabel *label;
@property (weak, nonatomic) UIView *backgroundCircle;

@end

@implementation RSGameZoomView

- (void)configureUI
{
    [self rs_addWidthConstraint:RSGameZoomViewDimension];
    [self rs_addHeightConstraint:RSGameZoomViewDimension];
    
    self.layer.cornerRadius = RSGameZoomViewDimension/2.0f;
    self.layer.borderWidth = 20.0;
    
    self.backgroundColor = [UIColor clearColor];
    self.translatesAutoresizingMaskIntoConstraints = NO;
    
    UIView *circle = [UIView new];
    circle.backgroundColor = [UIColor clearColor];
    circle.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:circle];
    
    [circle rs_addWidthConstraint:RSGameZoomViewDimension * 1.1];
    [circle rs_addHeightConstraint:RSGameZoomViewDimension * 1.1];
    [self rs_addCenterConstraintsToSubview:circle];
    
    circle.layer.cornerRadius = (RSGameZoomViewDimension * 1.1)/2.0f;
    circle.layer.borderWidth = (RSGameZoomViewDimension * 1.1)/2.0f;
    circle.layer.borderColor = [UIColor colorWithWhite:1.0 alpha:0.95].CGColor;
    
    self.backgroundCircle = circle;
    
    UILabel * label = [UILabel new];
    label.backgroundColor = [UIColor clearColor];
    label.translatesAutoresizingMaskIntoConstraints = NO;
    [self addSubview:label];
    
    [label rs_addWidthConstraint:roundf(RSGameZoomViewDimension * 0.5f * 1.4142f)];
    [label rs_addHeightConstraint:roundf(RSGameZoomViewDimension * 0.5f * 1.4142f)];
    [self rs_addCenterConstraintsToSubview:label];
    
    label.font = [UIFont fontWithName:@"Avenir-Black" size:70.0f];
    label.adjustsFontSizeToFitWidth = YES;
    label.minimumScaleFactor = 0.05;
    label.textAlignment = NSTextAlignmentCenter;
    label.numberOfLines = 0;
    
    self.label = label;
}

-(void)configureForSuccess
{
    [self configureUI];
    self.layer.borderColor = [UIColor rs_correctGreen].CGColor;
    self.label.textColor = [UIColor rs_correctGreen];
    
    switch (arc4random() % 4)
    {
        case 0:
            self.label.text = @"AWE SOME!";
            break;
        case 1:
            self.label.text = @"NICE WORK!";
            break;
        case 2:
            self.label.text = @"NICE!";
            break;
        default:
            self.label.text = @"GOOD JOB!";
            break;
    }
}

-(void)configureForFailure
{
    [self configureUI];
    self.layer.borderColor = [UIColor rs_incorrectRed].CGColor;
    self.label.textColor = [UIColor rs_incorrectRed];
    switch (arc4random() % 2)
    {
        case 0:
            self.label.text = @"NOT RIGHT!";
            break;
        default:
            self.label.text = @"TOO BAD!";
            break;
    }
}

-(void)configureForGameStart
{
    [self configureUI];
    
    self.backgroundCircle.layer.borderColor = [UIColor rs_gameHighlightColor].CGColor;
    
    self.layer.borderColor = [UIColor rs_rosettaYellowColor].CGColor;
    self.label.textColor = [UIColor rs_rosettaYellowColor];
    
    self.label.text = @"LET'S PLAY!";
}


@end
