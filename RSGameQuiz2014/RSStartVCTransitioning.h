//
//  RSStartVCTransitioning.h
//  RSGameQuiz2014
//
//  Created by David Coufal on 12/4/14.
//  Copyright (c) 2014 rosettastone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSStartVCTransitioning : NSObject<UIViewControllerAnimatedTransitioning>

@property (assign, nonatomic) BOOL presenting;

@end
