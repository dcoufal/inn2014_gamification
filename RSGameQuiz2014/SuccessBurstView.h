//
//  SuccessBurstView.h
//  RSGameQuiz2014
//
//  Created by David Coufal on 12/3/14.
//  Copyright (c) 2014 rosettastone. All rights reserved.
//

#import <UIKit/UIKit.h>

extern const CGFloat SuccessBurstViewDimension;

@interface SuccessBurstView : UIView

- (id)initWithCompletion:(void(^)(void))completion frame:(CGRect)frame;

@end
