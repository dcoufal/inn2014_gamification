//
//  4XSparkleView.m
//  RSGameQuiz2014
//
//  Created by David Coufal on 12/2/14.
//  Copyright (c) 2014 rosettastone. All rights reserved.
//

#import "4XSparkleView.h"

@implementation FourXSparkleView{
    __weak CAEmitterLayer *_sparkEmitter;
    CGFloat _decayAmount;
}

- (id)initWithFrame:(CGRect)frame {
    if ((self = [super initWithFrame:frame])) {
        self.userInteractionEnabled = NO;
        self.backgroundColor = [UIColor clearColor];
        
//        _sparkEmitter = (CAEmitterLayer*)self.layer;
//        _sparkEmitter.emitterPosition = CGPointMake(self.bounds.size.width /2, self.bounds.size.height /2);
//        _sparkEmitter.emitterSize = CGSizeMake(10.0, 10.0);
//        _sparkEmitter.emitterShape = kCAEmitterLayerCircle;
//        
//        CAEmitterCell *spark = [CAEmitterCell emitterCell];
//        spark.contents = (__bridge id)[[UIImage imageNamed:@"spark"] CGImage];
//        spark.name = @"spark";
//        spark.birthRate = 1;
//        spark.lifetime = 0.5;
//        spark.color = [[UIColor colorWithRed:1.0 green:0.97 blue:0.0 alpha:0.7] CGColor];
//        spark.redRange = 0.0;
//        spark.greenRange = 0.1;
//        spark.blueRange = 0.0;
//        
//        spark.velocity = 100;
//        spark.velocityRange = 20;
//        spark.emissionRange = (CGFloat) M_PI;
//        spark.emissionLongitude = (CGFloat) M_PI;
//        spark.scale = 1.0;
//        spark.scaleRange = 0.2;
//        spark.spinRange = 10.0;
//        _sparkEmitter.emitterCells = [NSArray arrayWithObject:spark];
        CAEmitterLayer *emitterLayer = (CAEmitterLayer*)self.layer;
        
        emitterLayer.name = @"emitterLayer";
        emitterLayer.emitterPosition = CGPointMake(self.bounds.size.width /2, self.bounds.size.height /2);
        emitterLayer.emitterZPosition = 0;
        
        emitterLayer.emitterSize = CGSizeMake(4.00, 4.00);
        emitterLayer.emitterDepth = 0.00;
        
        emitterLayer.renderMode = kCAEmitterLayerAdditive;
        
        emitterLayer.seed = 1175196832;
        
        
        
        
        // Create the emitter Cell
        CAEmitterCell *emitterCell = [CAEmitterCell emitterCell];
        
        emitterCell.name = @"untitled";
        emitterCell.enabled = YES;
        
        emitterCell.contents = (id)[[UIImage imageNamed:@"littlestar"] CGImage];
        emitterCell.contentsRect = CGRectMake(0.00, 0.00, 1.00, 1.00);
        
        emitterCell.magnificationFilter = kCAFilterLinear;
        emitterCell.minificationFilter = kCAFilterLinear;
        emitterCell.minificationFilterBias = 0.00;
        
        emitterCell.scale = 0.6;
        emitterCell.scaleRange = 0.00;
        emitterCell.scaleSpeed = 0.10;
        
        emitterCell.color = [[UIColor colorWithRed:0.50 green:0.5 blue:0.5 alpha:1.0] CGColor];
        emitterCell.redRange = 0.50;
        emitterCell.greenRange = 0.50;
        emitterCell.blueRange = 0.50;
        emitterCell.alphaRange = 0.00;
        
        emitterCell.redSpeed = 0.00;
        emitterCell.greenSpeed = 0.00;
        emitterCell.blueSpeed = 0.00;
        emitterCell.alphaSpeed = -0.00;
        
        emitterCell.lifetime = 0.60;
        emitterCell.lifetimeRange = 0.25;
        emitterCell.birthRate = 75;
        emitterCell.velocity = 250.00;
        emitterCell.velocityRange = 25.00;
        emitterCell.xAcceleration = 0.00;
        emitterCell.yAcceleration = 700.00;
        emitterCell.zAcceleration = 0.00;
        
        // these values are in radians, in the UI they are in degrees
        emitterCell.spin = 0.000;
        emitterCell.spinRange = 12.566;
        emitterCell.emissionLatitude = 0.000;
        emitterCell.emissionLongitude = 4.712;
        emitterCell.emissionRange = 6.283;
        
        
        
        emitterLayer.emitterCells = @[emitterCell];
    }
    
    return self;
}

+ (Class) layerClass {
    return [CAEmitterLayer class];
}

static NSTimeInterval const kDecayStepInterval = 0.1;
static float const finalbirthrate = 40;

- (void) spinUpStep {
    _sparkEmitter.birthRate +=_decayAmount;
    if (_sparkEmitter.birthRate > finalbirthrate) {
        _sparkEmitter.birthRate = finalbirthrate;
    } else {
        [self performSelector:@selector(spinUpStep) withObject:nil afterDelay:kDecayStepInterval];
    }
}

- (void) spinUpOverTime:(NSTimeInterval)interval
{
    _decayAmount = (CGFloat) (finalbirthrate /  (interval / kDecayStepInterval));
    [self spinUpStep];
}

- (void) decayStep {
    _sparkEmitter.birthRate -=_decayAmount;
    if (_sparkEmitter.birthRate < 0) {
        _sparkEmitter.birthRate = 0;
    } else {
        [self performSelector:@selector(decayStep) withObject:nil afterDelay:kDecayStepInterval];
    }
}

- (void) decayOverTime:(NSTimeInterval)interval {
    _decayAmount = (CGFloat) (_sparkEmitter.birthRate /  (interval / kDecayStepInterval));
    [self decayStep];
}

- (void) stopEmitting {
    _sparkEmitter.birthRate = 0.0;
}


@end
