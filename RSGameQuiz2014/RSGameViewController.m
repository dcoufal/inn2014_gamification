//
//  RSGameViewController.m
//  RSGameQuiz2014
//
//  Created by David Coufal on 12/1/14.
//  Copyright (c) 2014 rosettastone. All rights reserved.
//

#import "RSGameViewController.h"
#import "RSGame.h"
#import "RSGameScoreDisplayViewController.h"
#import "RSGameCountdownView.h"
#import "RSGameLabel.h"
#import "UIColor+RSGame.h"
#import "UIView+AutoLayout.h"
#import "UICountingLabel.h"
#import "4XSparkleView.h"
#import "RSGameZoomView.h"
#import "RSScoreVCTransitioning.h"
#import "SuccessBurstView.h"
#import "SadnessBurstView.h"

const static CGFloat standardAnimationDuration = 0.3f;


@interface RSGameViewController ()

@property (weak, nonatomic) IBOutlet UILabel *scoreMultiplierLevel;
@property (weak, nonatomic) IBOutlet UICountingLabel *scoreLabel;
@property (weak, nonatomic) IBOutlet RSGameCountdownView *timerProgress;
@property (weak, nonatomic) IBOutlet RSGameLabel *questionLabel;
@property (weak, nonatomic) IBOutlet UIButton *answer1Label;
@property (weak, nonatomic) IBOutlet UIButton *answer2Label;
@property (weak, nonatomic) IBOutlet UIButton *answer3Label;
@property (weak, nonatomic) IBOutlet UIButton *answer4Label;

@property (weak, nonatomic) FourXSparkleView *fourXSparkleView;

@property (assign, nonatomic) RSGameCountdownStatus countdownStatus;
@property (assign, nonatomic) RSGameMultipier multiplerCache;


@end

@implementation RSGameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES
                                            withAnimation:UIStatusBarAnimationFade];
    
    [self configUI];
    
    if (self.game == nil) {
        self.game = [RSGame new];
    }
    self.multiplerCache = RSGameMultipier1X;
}

- (void)viewWillAppear:(BOOL)animated
{
    [super viewWillAppear:animated];
    
    [self setupQuestion:NO shouldAutoStartQuiz:NO];
}

#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"gameToScoreView"])
    {
        RSGameScoreDisplayViewController *gameScoreVC = segue.destinationViewController;
        gameScoreVC.score = self.game.score;
        
        gameScoreVC.transitioningDelegate = self;
        gameScoreVC.modalPresentationStyle = UIModalPresentationCustom;
    }
}

-(void)startQuiz
{
    [self startQuestion];
}

- (void)setupQuestion:(BOOL)animated shouldAutoStartQuiz:(BOOL)autoStartQuiz
{
    __weak RSGameViewController *weakSelf = self;

    if (animated) {
        if (self.multiplerCache != self.game.multipler) {
            
            if (self.game.multipler == RSGameMultipier4X) {
                FourXSparkleView * view = [[FourXSparkleView alloc] initWithFrame:self.scoreMultiplierLevel.frame];
                view.translatesAutoresizingMaskIntoConstraints = NO;
                
                [self.view insertSubview:view atIndex:0];
                [self.view rs_addCenterConstraintsToSubview:view fromSubview:self.scoreMultiplierLevel];
                [view rs_addHeightConstraint:CGRectGetHeight(self.scoreMultiplierLevel.frame)];
                [view rs_addWidthConstraint:CGRectGetWidth(self.scoreMultiplierLevel.frame)];
                
                [view spinUpOverTime:0.1];
                self.fourXSparkleView = view;
                
                CABasicAnimation *anim = [CABasicAnimation animationWithKeyPath:@"transform.scale"];
                anim.timingFunction = [CAMediaTimingFunction functionWithName:kCAMediaTimingFunctionEaseInEaseOut];
                anim.duration=0.8;
                anim.repeatCount=HUGE_VALF;
                anim.autoreverses=YES;
                anim.fromValue=[NSNumber numberWithFloat:1.0];
                anim.toValue=[NSNumber numberWithFloat:1.1];
                [self.scoreMultiplierLevel.layer addAnimation:anim forKey:@"animatePulse"];
            }
            else {
                if (self.fourXSparkleView) {
                    [self.fourXSparkleView decayOverTime:0.1];
                    UIView *oldFourXView = self.fourXSparkleView;
                    self.fourXSparkleView = nil;
                    dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.15 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                        [oldFourXView removeFromSuperview];
                    });
                }
                
                [self.scoreMultiplierLevel.layer removeAllAnimations];
            }
            
            [UIView transitionWithView:self.scoreMultiplierLevel
                              duration:standardAnimationDuration
                               options:(self.game.multipler > self.multiplerCache) ? UIViewAnimationOptionTransitionFlipFromTop : UIViewAnimationOptionTransitionFlipFromBottom
                            animations:^{
                                self.scoreMultiplierLevel.text = self.game.multiplerText;
                            } completion:^(BOOL finished){
                                
                                UIImage *image = nil;
                                const CGFloat imgWidth = 81.0f;
                                const CGFloat imgHeight = 96.0f;
                                
                                if (self.game.multipler > self.multiplerCache) {
                                    image = [UIImage imageNamed:@"upstreak"];
                                }
                                else {
                                    image = [UIImage imageNamed:@"misstreak"];
                                }
                                UIImageView *imageView = [[UIImageView alloc] initWithImage:image];
                                imageView.translatesAutoresizingMaskIntoConstraints = NO;
                                [self.view addSubview:imageView];
                                [self.view rs_addCenterConstraintsToSubview:imageView fromSubview:self.scoreMultiplierLevel];
                                NSLayoutConstraint *width = [imageView rs_addWidthConstraint:imgWidth * 0.2];
                                NSLayoutConstraint *height = [imageView rs_addHeightConstraint:imgHeight * 0.2];
                                imageView.alpha = 0.0;
                                [self.view layoutIfNeeded];
                                
                                [UIView animateWithDuration:0.6
                                                      delay:0.0
                                     usingSpringWithDamping:0.4
                                      initialSpringVelocity:1.0
                                                    options:UIViewAnimationOptionCurveEaseInOut
                                                 animations:^{
                                                     height.constant = CGRectGetHeight(self.scoreMultiplierLevel.frame) - 8.0;
                                                     width.constant = (height.constant / imgHeight) * imgWidth;
                                                     imageView.alpha = 1.0;
                                                     [self.view layoutIfNeeded];
                                                 }
                                                 completion:^(BOOL finished){
                                                     [UIView animateWithDuration:0.3
                                                                           delay:0.0
                                                                         options:UIViewAnimationOptionCurveEaseInOut
                                                                      animations:^{
                                                                          imageView.transform = CGAffineTransformMakeScale(4.0, 4.0);
                                                                          imageView.alpha = 0.0;
                                                                      }
                                                                      completion:^(BOOL finished){
                                                                          [imageView removeFromSuperview];
                                                                      }];
                                                     
                                                 }];
                                
                                self.multiplerCache = self.game.multipler;
                            }];
            

        }
        
        [self.scoreLabel countFromCurrentValueTo:self.game.score withDuration:standardAnimationDuration];

        [UIView transitionWithView:self.questionLabel
                          duration:standardAnimationDuration
                           options:UIViewAnimationOptionTransitionCrossDissolve
                        animations:^{
                            self.questionLabel.text = self.game.questionText;
                        } completion:nil];
    }
    else {
        self.scoreMultiplierLevel.text = self.game.multiplerText;
        self.scoreLabel.text = self.game.scoreText;
        self.questionLabel.text = self.game.questionText;
    }

    [self setAnswerToButton:self.game.answer1Text button:self.answer1Label completion:nil animated:animated];
    [self setAnswerToButton:self.game.answer2Text button:self.answer2Label completion:nil animated:animated];
    [self setAnswerToButton:self.game.answer3Text button:self.answer3Label completion:nil animated:animated];
    [self setAnswerToButton:self.game.answer4Text button:self.answer4Label completion:^{
        
        if (autoStartQuiz) {
            [weakSelf startQuestion];
        }
        
    } animated:animated];
}

- (void)startQuestion
{
    if (self.game.endOfQuestions) {
        [self performSegueWithIdentifier:@"gameToScoreView" sender:self];
    }
    else {
        self.countdownStatus = RSGameCountdownStatusGreat;
        
        __weak RSGameViewController * weakSelf = self;
        
        [self.game startQuestion:^(float countdownValue, RSGameCountdownStatus countdownStatus){
            //            NSLog(@"progress:%f status:%lu", countdownValue, countdownStatus);
            
            RSGameViewController *strongSelf = weakSelf;
            
            if (strongSelf ) {
                strongSelf.timerProgress.countdown = countdownValue;
                
                if (strongSelf.countdownStatus != countdownStatus) {
                    [strongSelf changeCountdownStatus:countdownStatus];
                }
                
                if (countdownStatus == RSGameCountdownStatusGreat) {
                    strongSelf.timerProgress.countdownColor = [UIColor greenColor];
                }
                else if (countdownStatus == RSGameCountdownStatusFaster) {
                    strongSelf.timerProgress.countdownColor = [UIColor yellowColor];
                }
                else if (countdownStatus == RSGameCountdownStatusWeak) {
                    strongSelf.timerProgress.countdownColor = [UIColor redColor];
                }
                [strongSelf.timerProgress setNeedsDisplay];
            }
        }];
    }
}

- (void)setAnswerToButton:(NSString *)answer button:(UIButton *)button completion:(void(^)(void))completion animated:(BOOL)animated
{
    if (animated) {
        [UIView transitionWithView:button
                          duration:standardAnimationDuration
                           options:UIViewAnimationOptionTransitionFlipFromRight
                        animations:^{
                            
                            button.alpha = 0.0;
                            
                            
                        } completion:^(BOOL finished) {
                            [button setTitle:answer forState:UIControlStateNormal];
                            [self configureAnswerButtonColor:button];
                            
                            [UIView transitionWithView:button
                                              duration:standardAnimationDuration
                                               options:UIViewAnimationOptionTransitionFlipFromRight
                                            animations:^{
                                                
                                                button.alpha = 1.0;
                                                
                                            } completion:^(BOOL finished) {
                                                if (completion) {
                                                    completion();
                                                }
                                            }];
                        }];
    }
    else {
        [button setTitle:answer forState:UIControlStateNormal];
        if (completion) {
            completion();
        }
    }
}


-(void)changeCountdownStatus:(RSGameCountdownStatus)countdownStatus
{
    self.countdownStatus = countdownStatus;
    
    const CGFloat labelWidth = 200.0;
    const CGFloat labelHeight = 40.0f;

    UILabel *label = [[UILabel alloc] init];
    label.font = self.questionLabel.font;
    label.textColor = [UIColor rs_rosettaBlueColor];
    label.adjustsFontSizeToFitWidth = YES;
    label.minimumScaleFactor = 0.1;
    label.textAlignment = NSTextAlignmentCenter;
    switch (countdownStatus) {
        case RSGameCountdownStatusFaster:
            label.text = @"Hurry Up!";
            break;
        case RSGameCountdownStatusWeak:
            label.text = @"Go Faster!";
            break;
            
        default:
            break;
    }
    label.translatesAutoresizingMaskIntoConstraints = NO;
    [self.view addSubview:label];
    [self.view rs_addCenterConstraintsToSubview:label fromSubview:self.timerProgress];
    NSLayoutConstraint *width = [label rs_addWidthConstraint:labelWidth * 0.25];
    NSLayoutConstraint *height = [label rs_addHeightConstraint:labelHeight * 0.25];
    CGFloat rotation = (countdownStatus % 2) == 0 ? -25.0f/360.0f : 25.0f/360.0f;
    label.transform = CGAffineTransformMakeRotation(rotation);
    label.alpha = 0.0;
    [self.view layoutIfNeeded];
    
    [UIView animateWithDuration:0.6
                          delay:0.0
         usingSpringWithDamping:0.4
          initialSpringVelocity:1.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         height.constant = labelHeight;
                         width.constant = labelWidth;
                         label.alpha = 1.0;
                         [self.view layoutIfNeeded];
                     }
                     completion:^(BOOL finished){
                         [UIView animateWithDuration:0.3
                                               delay:0.0
                                             options:UIViewAnimationOptionCurveEaseInOut
                                          animations:^{
                                              label.transform = CGAffineTransformScale(label.transform, 4.0, 4.0);
                                              label.alpha = 0.0;
                                          }
                                          completion:^(BOOL finished){
                                              [label removeFromSuperview];
                                          }];
                         
                     }];
}

- (IBAction)answer1Tapped:(UIButton *)sender {
    [self answerQuestion:0 button:self.answer1Label];
}

- (IBAction)answer2Tapped:(UIButton *)sender {
    [self answerQuestion:1 button:self.answer2Label];
}

- (IBAction)answer3Tapped:(UIButton *)sender {
    [self answerQuestion:2 button:self.answer3Label];
}

- (IBAction)answer4Tapped:(UIButton *)sender {
    [self answerQuestion:3 button:self.answer4Label];
}

- (void)answerQuestion:(NSUInteger)answer button:(UIButton *)button
{
    BOOL correct = [self.game answer:answer];
    
    [UIView transitionWithView:button
                      duration:standardAnimationDuration
                       options:UIViewAnimationOptionTransitionFlipFromRight
                    animations:^{
                        
                        button.backgroundColor = correct ? [UIColor rs_correctGreen] : [UIColor rs_incorrectRed];
                        
                    } completion:^(BOOL finished) {
                        RSGameZoomView * zoomView = [RSGameZoomView new];
                        zoomView.transform = CGAffineTransformMakeScale(0.1, 0.1);
                        zoomView.alpha = 0.0f;
                        [self.view addSubview:zoomView];
                        [self.view rs_addCenterConstraintsToSubview:zoomView];
                        
                        if (correct) {
                            [zoomView configureForSuccess];
                        }
                        else {
                            [zoomView configureForFailure];
                        }
                        [self.view layoutIfNeeded];
                        
                        [UIView animateWithDuration:0.6
                                              delay:0.0
                             usingSpringWithDamping:0.4
                              initialSpringVelocity:1.0
                                            options:UIViewAnimationOptionCurveEaseInOut
                                         animations:^{
                                             zoomView.transform = CGAffineTransformRotate( CGAffineTransformMakeScale(0.6, 0.6), -.2);
                                             zoomView.alpha = 1.0;
                                             [self.view layoutIfNeeded];
                                         }
                                         completion:^(BOOL finished){
                                             
                                             UIView * animView = nil;
                                             if (correct) {
                                                 SuccessBurstView *successBurstView = [[SuccessBurstView alloc] initWithCompletion:^{
                                                     [successBurstView removeFromSuperview];
                                                 } frame:zoomView.frame];
                                                 animView = successBurstView;
                                             }
                                             else {
                                                 SadnessBurstView *sadnessBurstView = [[SadnessBurstView alloc] initWithCompletion:^{
                                                     [sadnessBurstView removeFromSuperview];
                                                 } frame:zoomView.frame];
                                                 animView = sadnessBurstView;
                                             }
                                             [self.view addSubview:animView];
                                             [self.view rs_addCenterConstraintsToSubview:animView];
                                             [animView rs_addHeightConstraint:SuccessBurstViewDimension];
                                             [animView rs_addWidthConstraint:SuccessBurstViewDimension];
                                             
                                             [UIView animateWithDuration:0.3
                                                                   delay:0.4
                                                                 options:UIViewAnimationOptionCurveEaseInOut
                                                              animations:^{
                                                                  zoomView.transform = CGAffineTransformRotate( CGAffineTransformMakeScale(2.0, 2.0), .6);
                                                                  zoomView.alpha = 0.0;
                                                                  [self.view layoutIfNeeded];
                                                              }
                                                              completion:^(BOOL finished){
                                                                  [zoomView removeFromSuperview];
                                                              }];
                                             
                                         }];
                        
                        dispatch_after(dispatch_time(DISPATCH_TIME_NOW, 0.5 * NSEC_PER_SEC), dispatch_get_main_queue(), ^{
                            [self setupQuestion:YES shouldAutoStartQuiz:YES];
                        });
                    }];
}




-(void)configUI
{
    self.view.backgroundColor = [UIColor rs_gameBackgroundColor];
    
    self.scoreLabel.format = @"%d";
    
    self.timerProgress.countdown = 0.0;
    self.timerProgress.countdownColor = [UIColor greenColor];
    
    [self.view configureView: self.scoreMultiplierLevel];
    self.scoreMultiplierLevel.textColor = [UIColor whiteColor];
    
    [self.view configureView: self.scoreLabel];
    self.scoreLabel.textColor = [UIColor whiteColor];
    
    [self.view configureView: self.timerProgress];
    
    [self.view configureView: self.questionLabel];
    self.questionLabel.textColor = [UIColor whiteColor];
    
    [self configureAnswerButton:self.answer1Label];
    [self configureAnswerButton:self.answer2Label];
    [self configureAnswerButton:self.answer3Label];
    [self configureAnswerButton:self.answer4Label];
}

-(void)configureAnswerButton:(UIButton *)button
{
    [self.view configureView:button];
    [self configureAnswerButtonColor:button];
    button.titleLabel.textColor = [UIColor whiteColor];
    button.titleLabel.textAlignment = NSTextAlignmentCenter;
    button.titleEdgeInsets = UIEdgeInsetsMake(0.0, 10.0, 0.0, 10.0);
}

-(void)configureAnswerButtonColor:(UIButton *)button
{
    UIColor *buttonColor = [UIColor colorWithRed:0.0 green:150.0/255.0 blue:204.0/255.0 alpha:1.0];
    button.backgroundColor = buttonColor;
}


- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                  presentingController:(UIViewController *)presenting
                                                                      sourceController:(UIViewController *)source {
    
    RSScoreVCTransitioning *animator = [RSScoreVCTransitioning new];
    animator.presenting = YES;
    return animator;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    RSScoreVCTransitioning *animator = [RSScoreVCTransitioning new];
    animator.presenting = NO;
    return animator;
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

@end
