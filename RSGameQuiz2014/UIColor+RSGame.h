//
//  UIColor+RSGame.h
//  RSGameQuiz2014
//
//  Created by David Coufal on 12/1/14.
//  Copyright (c) 2014 rosettastone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface UIColor (RSGame)

+ (UIColor *)rs_incorrectRed;
+ (UIColor *)rs_correctGreen;
+ (UIColor *)rs_rosettaYellowColor;
+ (UIColor *)rs_rosettaBlueColor;
+ (UIColor *)rs_gameBackgroundColor;
+ (UIColor *)rs_gameHighlightColor;

@end
