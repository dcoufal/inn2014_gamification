//
//  AppDelegate.h
//  RSGameQuiz2014
//
//  Created by David Coufal on 12/1/14.
//  Copyright (c) 2014 rosettastone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

