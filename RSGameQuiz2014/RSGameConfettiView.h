//
//  RSGameConfettiView.h
//  RSGameQuiz2014
//
//  Created by David Coufal on 12/2/14.
//  Copyright (c) 2014 rosettastone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface RSGameConfettiView : UIView

- (void) spinUpAndDecayOver:(NSTimeInterval)interval;

//- (void) spinUpOverTime:(NSTimeInterval)interval;
//- (void) decayOverTime:(NSTimeInterval)interval;
//- (void) stopEmitting;

@end
