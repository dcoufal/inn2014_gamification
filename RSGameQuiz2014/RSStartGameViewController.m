//
//  RSStartGameViewController.m
//  RSGameQuiz2014
//
//  Created by David Coufal on 12/1/14.
//  Copyright (c) 2014 rosettastone. All rights reserved.
//

#import "RSStartGameViewController.h"
#import "RSGameViewController.h"
#import "RSGame.h"
#import "UIColor+RSGame.h"
#import "UIView+AutoLayout.h"
#import "RSGameVCTransitioning.h"

@interface RSStartGameViewController ()

@property (weak, nonatomic) IBOutlet UIButton *startFullGameButton;
@property (weak, nonatomic) IBOutlet UIButton *startShortGameButton;
@property (strong, nonatomic) RSGame *preconfiguredGame;
@property (weak, nonatomic) IBOutlet UIImageView *rsStoneImageView;
@property (weak, nonatomic) IBOutlet UIImageView *rsNameImageView;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *shortToFullConstraint;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *shortToBottomConstraint;
@property (weak, nonatomic) IBOutlet UILabel *titleLabel;

@end

@implementation RSStartGameViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    [[UIApplication sharedApplication] setStatusBarHidden:YES
                                            withAnimation:UIStatusBarAnimationFade];
    
    self.view.backgroundColor = [UIColor rs_gameBackgroundColor];
    
    [self configureButton:self.startFullGameButton];
    [self configureButton:self.startShortGameButton];

    self.rsStoneImageView.tintColor = [UIColor rs_rosettaBlueColor];
    self.rsNameImageView.tintColor = [UIColor rs_rosettaYellowColor];
    
    self.rsNameImageView.image = [self.rsNameImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    self.rsStoneImageView.image = [self.rsStoneImageView.image imageWithRenderingMode:UIImageRenderingModeAlwaysTemplate];
    
    self.shortToFullConstraint.constant = 0.0f;
    self.shortToBottomConstraint.constant = -40.0f;
    
    self.titleLabel.alpha = 0.0f;
    self.titleLabel.textColor = [UIColor rs_rosettaYellowColor];
}

- (void) viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [UIView animateWithDuration:1.2
                          delay:0.5
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.titleLabel.alpha = 1.0;
                     }
                     completion:nil];
    
    [UIView animateWithDuration:1.2
                          delay:0.0
         usingSpringWithDamping:0.4
          initialSpringVelocity:1.0
                        options:UIViewAnimationOptionCurveEaseInOut
                     animations:^{
                         self.shortToFullConstraint.constant = 23.0f;
                         self.shortToBottomConstraint.constant = 20.0f;
                         [self.view layoutIfNeeded];
                     }
                     completion:nil];
}

-(void)configureButton:(UIButton *)button
{
    [self.view configureView: button];
    [button setTitleColor:[UIColor rs_rosettaYellowColor] forState:UIControlStateNormal];
    button.layer.borderColor = [UIColor rs_rosettaYellowColor].CGColor;
    button.backgroundColor = [UIColor rs_gameHighlightColor];
}

- (IBAction)returnToStart:(UIStoryboardSegue *)segue
{
    
}

- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    if ([[segue identifier] isEqualToString:@"startTheGame"])
    {
        RSGameViewController *gameVC = segue.destinationViewController;
        gameVC.game = self.preconfiguredGame;
        self.preconfiguredGame = nil;
        
        gameVC.transitioningDelegate = self;
        gameVC.modalPresentationStyle = UIModalPresentationCustom;
    }
}

- (IBAction)startFullGameTapped:(UIButton *)sender {
}

- (IBAction)startShortGameTapped:(UIButton *)sender {
    self.preconfiguredGame = [[RSGame alloc] initWithNumberOfQuestions:2];
    [self performSegueWithIdentifier:@"startTheGame" sender:self];
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForPresentedController:(UIViewController *)presented
                                                                  presentingController:(UIViewController *)presenting
                                                                      sourceController:(UIViewController *)source {
    
    RSGameVCTransitioning *animator = [RSGameVCTransitioning new];
    animator.presenting = YES;
    return animator;
}

- (id<UIViewControllerAnimatedTransitioning>)animationControllerForDismissedController:(UIViewController *)dismissed {
    RSGameVCTransitioning *animator = [RSGameVCTransitioning new];
    animator.presenting = NO;
    return animator;
}

- (BOOL)prefersStatusBarHidden {
    return YES;
}

@end
