//
//  4XSparkleView.h
//  RSGameQuiz2014
//
//  Created by David Coufal on 12/2/14.
//  Copyright (c) 2014 rosettastone. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface FourXSparkleView : UIView

- (void) spinUpOverTime:(NSTimeInterval)interval;
- (void) decayOverTime:(NSTimeInterval)interval;

@end
