//
//  AriaMobile
//
//  Created by Jesse Squires on 10/23/13.
//  Copyright (c) 2013 Rosetta Stone. All rights reserved.
//

#import "UIView+AutoLayout.h"
#import "UIColor+RSGame.h"

const CGFloat cornerRadius = 8.0f;
const CGFloat borderWidth = 3.0f;

@implementation UIView (AutoLayout)

- (void)rs_addEqualConstraintToSubview:(UIView *)view withAttribute:(NSLayoutAttribute)attribute
{
    [self addConstraint:[NSLayoutConstraint constraintWithItem:self
                                                     attribute:attribute
                                                     relatedBy:NSLayoutRelationEqual
                                                        toItem:view
                                                     attribute:attribute
                                                    multiplier:1.0f
                                                      constant:0.0f]];
}

- (void)rs_addEqualContentConstraintsToAllEdgesOfSubview:(UIView *)view
{
    [self rs_addEqualConstraintToSubview:view withAttribute:NSLayoutAttributeBottom];
    [self rs_addEqualConstraintToSubview:view withAttribute:NSLayoutAttributeTop];
    [self rs_addEqualConstraintToSubview:view withAttribute:NSLayoutAttributeLeading];
    [self rs_addEqualConstraintToSubview:view withAttribute:NSLayoutAttributeTrailing];
}

- (void)rs_addCenterConstraintsToSubview:(UIView *)view
{
    
    NSLayoutConstraint *cHorizCenter =[NSLayoutConstraint constraintWithItem:view
                                                                   attribute:NSLayoutAttributeCenterX
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:self
                                                                   attribute:NSLayoutAttributeCenterX
                                                                  multiplier:1
                                                                    constant:0];
    
    NSLayoutConstraint *cVertCenter =[NSLayoutConstraint constraintWithItem:view
                                                                   attribute:NSLayoutAttributeCenterY
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:self
                                                                   attribute:NSLayoutAttributeCenterY
                                                                  multiplier:1
                                                                    constant:0];

    [self addConstraints:@[cHorizCenter, cVertCenter]];
}

- (void)rs_addCenterConstraintsToSubview:(UIView *)view fromSubview:(UIView *)masterSubView
{
    
    NSLayoutConstraint *cHorizCenter =[NSLayoutConstraint constraintWithItem:view
                                                                   attribute:NSLayoutAttributeCenterX
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:masterSubView
                                                                   attribute:NSLayoutAttributeCenterX
                                                                  multiplier:1
                                                                    constant:0];
    
    NSLayoutConstraint *cVertCenter =[NSLayoutConstraint constraintWithItem:view
                                                                  attribute:NSLayoutAttributeCenterY
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:masterSubView
                                                                  attribute:NSLayoutAttributeCenterY
                                                                 multiplier:1
                                                                   constant:0];
    
    [self addConstraints:@[cHorizCenter, cVertCenter]];
    
}

- (NSLayoutConstraint *)rs_addWidthConstraint:(CGFloat)width
{
    NSLayoutConstraint *cWidth =[NSLayoutConstraint constraintWithItem:self
                                                             attribute:NSLayoutAttributeWidth
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:nil
                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                            multiplier:1
                                                              constant:width];
    [self addConstraint:cWidth];
    return cWidth;
}

- (NSLayoutConstraint *)rs_addHeightConstraint:(CGFloat)height
{
    NSLayoutConstraint *cHeight =[NSLayoutConstraint constraintWithItem:self
                                                             attribute:NSLayoutAttributeHeight
                                                             relatedBy:NSLayoutRelationEqual
                                                                toItem:nil
                                                             attribute:NSLayoutAttributeNotAnAttribute
                                                            multiplier:1
                                                              constant:height];
    [self addConstraint:cHeight];
    return cHeight;
}

- (void)rs_addHorizontalCenterConstraintToSubview:(UIView *)view
{
    
    NSLayoutConstraint *cHorizCenter =[NSLayoutConstraint constraintWithItem:view
                                                                   attribute:NSLayoutAttributeCenterX
                                                                   relatedBy:NSLayoutRelationEqual
                                                                      toItem:self
                                                                   attribute:NSLayoutAttributeCenterX
                                                                  multiplier:1
                                                                    constant:0];
    
    
    [self addConstraint:cHorizCenter];
}

- (void)rs_addVerticalCenterConstraintToSubview:(UIView *)view
{
    
    NSLayoutConstraint *cVertCenter =[NSLayoutConstraint constraintWithItem:view
                                                                  attribute:NSLayoutAttributeCenterY
                                                                  relatedBy:NSLayoutRelationEqual
                                                                     toItem:self
                                                                  attribute:NSLayoutAttributeCenterY
                                                                 multiplier:1
                                                                   constant:0];
    
    [self addConstraint:cVertCenter];
}

- (NSLayoutConstraint *)rs_addTopConstraint:(float)topMargin toSubview:(UIView *)view
{
    NSLayoutConstraint *top = [NSLayoutConstraint constraintWithItem:view
                                                           attribute:NSLayoutAttributeTop
                                                           relatedBy:NSLayoutRelationEqual
                                                              toItem:self
                                                           attribute:NSLayoutAttributeTop
                                                          multiplier:1
                                                            constant:topMargin];
    
    [self addConstraint:top];
    return top;
}











-(void)configureView:(UIView *)view
{
    UIColor *edgeColor = [UIColor rs_gameHighlightColor];
    view.layer.cornerRadius = cornerRadius;
    view.layer.borderWidth = borderWidth;
    view.layer.borderColor = edgeColor.CGColor;
    view.backgroundColor = [UIColor clearColor];
}


@end
