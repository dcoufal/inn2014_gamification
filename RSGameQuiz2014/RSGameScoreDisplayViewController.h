//
//  RSGameScoreDisplayViewController.h
//  RSGameQuiz2014
//
//  Created by David Coufal on 12/1/14.
//  Copyright (c) 2014 rosettastone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "UICountingLabel.h"

@interface RSGameScoreDisplayViewController : UIViewController

@property (assign, nonatomic) NSUInteger score;
@property (weak, nonatomic) IBOutlet UICountingLabel *scoreDisplayLabel;

@end
