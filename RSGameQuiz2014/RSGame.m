//
//  RSGame.m
//  RSGameQuiz2014
//
//  Created by David Coufal on 12/1/14.
//  Copyright (c) 2014 rosettastone. All rights reserved.
//

#import <QuartzCore/QuartzCore.h>
#import "RSGame.h"

const static NSUInteger kNumQuestions = 10;
const static NSUInteger kBaseQuestionScoreValue = 10000;

typedef NS_ENUM(NSUInteger, RSInternalGameCountdownStatus) {
    RSInternalGameCountdownStatus_InitialSlowCountdown_Great,
    RSInternalGameCountdownStatus_FasterCountdown_Great,
    RSInternalGameCountdownStatus_Faster,
    RSInternalGameCountdownStatus_Weak,
};

const static CGFloat Time_InitialSlowCountdown_Great = 0.8f;
const static CGFloat Time_FasterCountdown_Great = 1.8f;
const static CGFloat Time_Faster = 3.8f;
const static CGFloat Time_Weak = 7.0f;

const static CGFloat Score_InitialSlowCountdown_Great = 0.95f;
const static CGFloat Score_FasterCountdown_Great = 0.85f;
const static CGFloat Score_Faster = 0.6f;
const static CGFloat Score_Weak = 0.2f;



@interface RSGame ()

@property (assign, nonatomic) RSGameMultipier multipler;

@property (assign, nonatomic) NSUInteger score;

@property (assign, nonatomic) NSUInteger currentQuestionNumber;

@property (readonly, nonatomic) NSArray *questions;
@property (readonly, nonatomic) NSArray *answers1;
@property (readonly, nonatomic) NSArray *answers2;
@property (readonly, nonatomic) NSArray *answers3;
@property (readonly, nonatomic) NSArray *answers4;
@property (readonly, nonatomic) NSArray *correctAnswers;
@property (readonly, nonatomic) NSUInteger numQuestions;

@property (assign, nonatomic) CGFloat scoreTimePenalty;
@property (strong, nonatomic) NSTimer *timer;
@property (assign, nonatomic) RSInternalGameCountdownStatus internalTimerStatus;
@property (assign, nonatomic) NSTimeInterval startTime;
@property (nonatomic, copy) void (^countdownCallback)(float countdownValue, RSGameCountdownStatus countdownStatus);

@end

@implementation RSGame

-(instancetype)initWithNumberOfQuestions:(NSUInteger)numberOfQuestions
{
    self = [super init];
    if (self) {
        [self configure];
        _numQuestions = MAX(MIN(numberOfQuestions, kNumQuestions), 1);
    }
    return self;
}

-(instancetype)init
{
    self = [super init];
    if (self) {
        [self configure];
        _numQuestions = kNumQuestions;
    }
    return self;
}

-(void)configure
{
    _multipler = RSGameMultipier1X;
    _score = 0;
    _currentQuestionNumber = 0;
    [self initQA];
}

-(void)startQuestion:(void(^)(float countdownValue, RSGameCountdownStatus countdownStatus))callback
{
    self.scoreTimePenalty = 1.0f;
    self.internalTimerStatus = RSInternalGameCountdownStatus_InitialSlowCountdown_Great;
    self.countdownCallback = callback;
    [self startTimer];
}

-(void)startTimer
{
    self.startTime = CACurrentMediaTime();
    [self stopTimer];
    self.timer = [NSTimer scheduledTimerWithTimeInterval:0.03
                                                  target:self
                                                selector:@selector(timerFired:)
                                                userInfo:nil
                                                 repeats:YES];
}

-(void)stopTimer
{
    if (self.timer) {
        [self.timer invalidate];
        self.timer = nil;
    }
}

-(void)timerFired:(NSTimer *)timer
{
    dispatch_async(dispatch_get_main_queue(), ^{
        NSTimeInterval time = CACurrentMediaTime() - self.startTime;
        switch (self.internalTimerStatus) {
            case RSInternalGameCountdownStatus_InitialSlowCountdown_Great:
            {
                [self calculateScoreTimePenalty:time maxTime:Time_InitialSlowCountdown_Great minScore:Score_InitialSlowCountdown_Great maxScore:1.0];
            }
                break;
            case RSInternalGameCountdownStatus_FasterCountdown_Great:
            {
                [self calculateScoreTimePenalty:time maxTime:Time_FasterCountdown_Great minScore:Score_FasterCountdown_Great maxScore:Score_InitialSlowCountdown_Great];
            }
                break;
            case RSInternalGameCountdownStatus_Faster:
            {
                [self calculateScoreTimePenalty:time maxTime:Time_Faster minScore:Score_Faster maxScore:Score_FasterCountdown_Great];
            }
                break;
            case RSInternalGameCountdownStatus_Weak:
            {
                [self calculateScoreTimePenalty:time maxTime:Time_Weak minScore:Score_Weak maxScore:Score_Faster];
            }
                break;
            default:
                [self stopTimer];
                break;
        }
    });
}

-(void)calculateScoreTimePenalty:(CGFloat)time maxTime:(CGFloat)maxTime minScore:(CGFloat)minScore maxScore:(CGFloat)maxScore
{
    if (time > maxTime) {

        self.scoreTimePenalty = minScore;
        
        [self stopTimer];
        
        if (self.internalTimerStatus == RSInternalGameCountdownStatus_Weak) {
            return; // end of the road...
        }
        
        self.internalTimerStatus++;
        if (self.countdownCallback) {
            self.countdownCallback(self.scoreTimePenalty, [self transformInternalCountdownStatus:self.internalTimerStatus]);
        }
        dispatch_async(dispatch_get_main_queue(), ^{
            [self startTimer];
        });
    }
    
    self.scoreTimePenalty = (((maxTime - time) / maxTime) * (maxScore - minScore)) + minScore;
    if (self.countdownCallback) {
        self.countdownCallback(self.scoreTimePenalty, [self transformInternalCountdownStatus:self.internalTimerStatus]);
    }
}

-(RSGameCountdownStatus)transformInternalCountdownStatus:(RSInternalGameCountdownStatus)intStatus
{
    switch (intStatus) {
        case RSInternalGameCountdownStatus_Weak:
            return RSGameCountdownStatusWeak;
            break;
        case RSInternalGameCountdownStatus_Faster:
            return RSGameCountdownStatusFaster;
            break;
        default:
            return RSGameCountdownStatusGreat;
            break;
    }
}
                                   

-(BOOL)answer:(NSUInteger)answerNumber
{
    [self stopTimer];
    
    NSNumber *correctAnswerNumber = self.correctAnswers[self.currentQuestionNumber];
    BOOL answeredCorrectly = (answerNumber == correctAnswerNumber.integerValue);
    
    if (answeredCorrectly) {
        self.score += (self.multipler + 1) * [self questionScoreValue];
        if (self.multipler < RSGameMultipier4X) {
            self.multipler++;
        }
    }
    else {
        self.multipler = RSGameMultipier1X;
    }
    
    self.currentQuestionNumber++;
    
    return answeredCorrectly;
}

-(NSString *)multiplerText
{
    switch (self.multipler) {
        case RSGameMultipier2X:
            return @"2X";
            break;
        case RSGameMultipier3X:
            return @"3X";
            break;
        case RSGameMultipier4X:
            return @"4X";
            break;
        default:
            return @"1X";
            break;
    }
}

-(NSString *)scoreText
{
    return [NSString stringWithFormat:@"%lu", (unsigned long)self.score];
}

-(BOOL)endOfQuestions
{
    return self.currentQuestionNumber >= self.numQuestions;
}

-(NSUInteger)questionScoreValue
{
    return self.scoreTimePenalty * kBaseQuestionScoreValue;
}

-(NSString *)questionText
{
    if ([self endOfQuestions]) {
        return @"";
    }
    return self.questions[self.currentQuestionNumber];
}

-(NSString *)answer1Text
{
    if ([self endOfQuestions]) {
        return @"";
    }
    return self.answers1[self.currentQuestionNumber];
}

-(NSString *)answer2Text
{
    if ([self endOfQuestions]) {
        return @"";
    }
    return self.answers2[self.currentQuestionNumber];
}

-(NSString *)answer3Text
{
    if ([self endOfQuestions]) {
        return @"";
    }
    return self.answers3[self.currentQuestionNumber];
}

-(NSString *)answer4Text
{
    if ([self endOfQuestions]) {
        return @"";
    }
    return self.answers4[self.currentQuestionNumber];
}

-(void)initQA
{
    _questions = @[ @"Translate this phrase:\nQuiero una cerveza",
                    @"Translate this phrase:\nVerde es mi color favorito",
                    @"Translate this phrase:\nMi vuelo sale en una hora",
                    @"Translate this phrase:\nTengo una reserva",
                    @"Translate this phrase:\n¿Dónde está el baño?",
                    @"Translate this phrase:\nTengo que cambiar dinero",
                    @"Translate this phrase:\nLa cuenta, por favor",
                    @"Translate this phrase:\nNecesito ir al aeropuerto",
                    @"Translate this phrase:\n¿Cómo te va?",
                    @"Translate this phrase:\n¿Cuánto cuesta?",];
    
    _answers1 = @[ @"I want a beer",
                   @"The wall is green",
                   @"My flight is late",
                   @"Please make a reservation",
                   @"Where is the bathroom?",
                   @"Can you give me some money?",
                   @"Can I get the menu, please",
                   @"Where is the airport",
                   @"How’s it going?",
                   @"Can I buy this?",];
    
    _answers2 = @[ @"Where is the beer",
                   @"Green is my favorite color",
                   @"The bus leaves in one hour",
                   @"Do you have a reservation?",
                   @"Where is the library?",
                   @"I need to exchange money",
                   @"Napkins, please",
                   @"How can I get to the airport",
                   @"Who is that?",
                   @"How much is it?",];
    
    _answers3 = @[ @"Where can I get some beer",
                   @"Green is your favorite color",
                   @"My flight leaves in one hour",
                   @"Reservations are required",
                   @"Where is the railway station?",
                   @"Would you like some money?",
                   @"Check please",
                   @"We are at the airport",
                   @"What are you doing?",
                   @"Where can I find this?",];
    
    _answers4 = @[ @"The beer is in the glass",
                   @"The leaves are green.",
                   @"The flight left one hour ago",
                   @"I have a reservation",
                   @"Where is the kitchen?",
                   @"I need to go to the bank.",
                   @"Beer, please",
                   @"I need to go to the airport",
                   @"Do you want to go?",
                   @"Is it in the store?",];
    
    _correctAnswers = @[ @0,
                         @1,
                         @2,
                         @3,
                         @0,
                         @1,
                         @2,
                         @3,
                         @0,
                         @1];
    
}

@end
