//
//  RSScoreVCTransitioning.m
//  RSGameQuiz2014
//
//  Created by David Coufal on 12/2/14.
//  Copyright (c) 2014 rosettastone. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "RSScoreVCTransitioning.h"
#import "UIView+AutoLayout.h"
#import "UIColor+RSGame.h"

@implementation RSScoreVCTransitioning

- (NSTimeInterval)transitionDuration:(id <UIViewControllerContextTransitioning>)transitionContext {
    return 0.5f;
}

- (void)animateTransition:(id <UIViewControllerContextTransitioning>)transitionContext {
    // Grab the from and to view controllers from the context
    UIViewController *fromViewController = [transitionContext viewControllerForKey:UITransitionContextFromViewControllerKey];
    UIViewController *toViewController = [transitionContext viewControllerForKey:UITransitionContextToViewControllerKey];
    
    if (self.presenting) {
        [transitionContext.containerView addSubview:toViewController.view];
        
        // take a screenshot of our source VC and display in Image View
        UIGraphicsBeginImageContextWithOptions(fromViewController.view.bounds.size, NO, [UIScreen mainScreen].scale);
        
        [fromViewController.view drawViewHierarchyInRect:fromViewController.view.bounds afterScreenUpdates:YES];
        
        UIImage *imageSource = UIGraphicsGetImageFromCurrentImageContext();
        UIGraphicsEndImageContext();
        
        UIImageView *imageSourceView = [[UIImageView alloc] initWithImage:imageSource];
        imageSourceView.translatesAutoresizingMaskIntoConstraints = NO;
        [transitionContext.containerView addSubview:imageSourceView];
        [transitionContext.containerView rs_addEqualContentConstraintsToAllEdgesOfSubview:imageSourceView];
        
        UIBezierPath *initialPath = [self makePathWithRadius:1.0f parentView:fromViewController.view];
        UIBezierPath *finalPath = [self makePathWithRadius:330.0f parentView:fromViewController.view];
        
        CAShapeLayer *maskLayer = [CAShapeLayer layer];
        maskLayer.path = initialPath.CGPath;
        maskLayer.fillRule = kCAFillRuleEvenOdd;
        maskLayer.fillColor = [UIColor blackColor].CGColor;
        maskLayer.opacity = 1.0;
        imageSourceView.layer.mask = maskLayer;
        
        [transitionContext.containerView layoutIfNeeded];
        
        [CATransaction begin]; {
            [CATransaction setCompletionBlock:^{
                [imageSourceView removeFromSuperview];
                [transitionContext completeTransition:YES];
            }];
            CABasicAnimation *pathAppear = [CABasicAnimation animationWithKeyPath:@"path"];
            pathAppear.duration = 1.2;
            pathAppear.fromValue = (__bridge id)initialPath.CGPath;
            pathAppear.toValue = (__bridge id)finalPath.CGPath;
            
            [maskLayer addAnimation:pathAppear forKey:@"pathanim"];
        }
    }
    else {
        [transitionContext completeTransition:YES];
    }
    
}

- (UIBezierPath *)makePathWithRadius:(CGFloat)radius parentView:(UIView *)view
{
    UIBezierPath *path = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(0,
                                                                            0,
                                                                            CGRectGetWidth(view.bounds),
                                                                            CGRectGetHeight(view.bounds))
                                                    cornerRadius:0];
    UIBezierPath *circlePath = [UIBezierPath bezierPathWithRoundedRect:CGRectMake(CGRectGetWidth(view.bounds) / 2.0 - radius,
                                                                                  CGRectGetHeight(view.bounds) / 2.0 - radius,
                                                                                  2.0*radius,
                                                                                  2.0*radius)
                                                          cornerRadius:radius];
    [path appendPath:circlePath];
    [path setUsesEvenOddFillRule:YES];
    return path;
}

@end
