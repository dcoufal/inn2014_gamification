//
//  RSGameCountdownView.m
//  RSGameQuiz2014
//
//  Created by David Coufal on 12/1/14.
//  Copyright (c) 2014 rosettastone. All rights reserved.
//

#import "RSGameCountdownView.h"

@implementation RSGameCountdownView

// Only override drawRect: if you perform custom drawing.
// An empty implementation adversely affects performance during animation.
- (void)drawRect:(CGRect)rect {
    
    rect = CGRectInset(rect, 3.0, 3.0);
    
    CGFloat fencedCountdown = MIN(MAX(self.countdown, 0.0), 1.0);
    CGFloat width = roundf(CGRectGetWidth(rect) * fencedCountdown);
    
    CGRect countdownrect = CGRectMake(CGRectGetWidth(rect) - width + 3.0,
                                      3.0,
                                      width,
                                      CGRectGetHeight(rect));
    
    CGContextRef context = UIGraphicsGetCurrentContext();
    CGContextSetFillColorWithColor(context, self.countdownColor.CGColor);
    CGContextSetStrokeColorWithColor(context, self.countdownColor.CGColor);
    CGContextFillRect(context, countdownrect);
}

@end
