//
//  AriaMobile
//
//  Created by Jesse Squires on 10/23/13.
//  Copyright (c) 2013 Rosetta Stone. All rights reserved.
//

#import <UIKit/UIKit.h>

extern const CGFloat cornerRadius;
extern const CGFloat borderWidth;

@interface UIView (AutoLayout)

- (void)rs_addEqualConstraintToSubview:(UIView *)view withAttribute:(NSLayoutAttribute)attribute;

- (void)rs_addEqualContentConstraintsToAllEdgesOfSubview:(UIView *)view;

- (void)rs_addCenterConstraintsToSubview:(UIView *)view;
- (void)rs_addCenterConstraintsToSubview:(UIView *)view fromSubview:(UIView *)masterSubView;
- (void)rs_addHorizontalCenterConstraintToSubview:(UIView *)view;
- (void)rs_addVerticalCenterConstraintToSubview:(UIView *)view;

- (NSLayoutConstraint *)rs_addWidthConstraint:(CGFloat)width;
- (NSLayoutConstraint *)rs_addHeightConstraint:(CGFloat)height;
- (NSLayoutConstraint *)rs_addTopConstraint:(float)topMargin toSubview:(UIView *)view;



-(void)configureView:(UIView *)view;

@end
