//
//  UIColor+RSGame.m
//  RSGameQuiz2014
//
//  Created by David Coufal on 12/1/14.
//  Copyright (c) 2014 rosettastone. All rights reserved.
//

#import "UIColor+RSGame.h"

@implementation UIColor (RSGame)

#pragma mark - Colors



+ (UIColor *)rs_incorrectRed
{
    return [UIColor colorWithRed:209.0/255.0
                           green:61.0/255.0
                            blue:37.0/255.0
                           alpha:1.0f];
}

+ (UIColor *)rs_correctGreen
{
    return [UIColor colorWithRed:28.0/255.0
                           green:222.0/255.0
                            blue:84.0/255.0
                           alpha:1.0f];
}

+ (UIColor *)rs_rosettaYellowColor
{
    return [UIColor colorWithRed:255.0/255.0
                           green:219.0/255.0
                            blue:53.0/255.0
                           alpha:1.0f];
}

+ (UIColor *)rs_rosettaBlueColor
{
    return [UIColor colorWithRed:52.0/255.0
                           green:164.0/255.0
                            blue:231.0/255.0
                           alpha:1.0f];
}

+ (UIColor *)rs_gameBackgroundColor
{
    return [UIColor colorWithRed:0.0 green:37.0/255.0 blue:51.0/255.0 alpha:1.0];;
}

+ (UIColor *)rs_gameHighlightColor
{
    return [UIColor colorWithRed:0.0 green:100.0/255.0 blue:140.0/255.0 alpha:1.0];
}

//
//+ (UIColor *)rs_rosettaYellowColor
//{
//    return [UIColor colorWithIntRed:255
//                              green:219
//                               blue:53
//                              alpha:1.0f];
//}
//
//+ (UIColor *)rs_rosettaBlueColor
//{
//    return [UIColor colorWithIntRed:52
//                              green:164
//                               blue:231
//                              alpha:1.0f];
//}
//
//+ (UIColor *)rs_rosettaDarkBlueColor
//{
//    return [UIColor colorWithIntRed:57
//                              green:147
//                               blue:213
//                              alpha:1.0f];
//}
//
//+ (UIColor *)rs_darkGrayColor
//{
//    return [UIColor colorWithIntRed:43
//                              green:51
//                               blue:57
//                              alpha:1.0f];
//}
//
//+ (UIColor *)rs_mediumGrayColor
//{
//    return [UIColor colorWithWhite:228.0f/255.0f alpha:1.0f];
//}
//
//+ (UIColor *)rs_lightGrayColor
//{
//    return [UIColor colorWithWhite:244.0/255.0f alpha:1.0f];
//}
//
//+ (UIColor *)rs_blackColor
//{
//    return [UIColor colorWithWhite:68.0f/255.0f alpha:1.0f];
//}
//
//+ (UIColor *)rs_successGreenColor
//{
//    return [UIColor colorWithIntRed:140
//                              green:200
//                               blue:50
//                              alpha:1.0f];
//}
//
//+ (UIColor *)rs_correctFeedbackDarkBlueColor
//{
//    return [UIColor colorWithIntRed:1
//                              green:134
//                               blue:216
//                              alpha:1.0f];
//}
//
//+ (UIColor *)rs_incorrectRedColor
//{
//    return [UIColor colorWithIntRed:212
//                              green:60
//                               blue:30
//                              alpha:1.0f];
//}
//
//+ (UIColor *)rs_incorrectPinkColor
//{
//    return [UIColor colorWithIntRed:252
//                              green:221
//                               blue:222
//                              alpha:1.0f];
//}
//
//+ (UIColor *)rs_lightYellowHighlightColor
//{
//    return [UIColor colorWithIntRed:255
//                              green:246
//                               blue:207
//                              alpha:1.0f];
//}
//
//+ (UIColor *)rs_darkYellowHighlightColor
//{
//    return [UIColor colorWithIntRed:255
//                              green:232
//                               blue:127
//                              alpha:1.0f];
//}
//
//#pragma mark - UI
//
//+ (UIColor *)rs_activityBackgroundColor
//{
//    return [UIColor rs_lightGrayColor];
//}
//
//+ (UIColor *)rs_activityWrapperBottomBackgroundColor
//{
//    return [UIColor rs_mediumGrayColor];
//}
//
//+ (UIColor *)rs_buttonNormalTextColor
//{
//    return [UIColor whiteColor];
//}
//
//+ (UIColor *)rs_buttonDisabledTextColor
//{
//    return [UIColor colorWithWhite:1.0f alpha:0.5f];
//}
//
//+ (UIColor *)rs_defaultTextColor
//{
//    return [UIColor rs_darkGrayColor];
//}
//
//+ (UIColor *)rs_lightGrayTextColor
//{
//    return [UIColor colorWithWhite:153.0f/255.0f alpha:1.0f];
//}
//
//+ (UIColor *)rs_lightGrayBlurOverlayTextColor
//{
//    return [UIColor colorWithWhite:118.0f/255.0f alpha:1.0f];
//}
//
//+ (UIColor *)rs_progressTrackTintColor
//{
//    return [UIColor colorWithIntRed:234 green:238 blue:240 alpha:1.0f];
//}
//
//+ (UIColor *)rs_readAloudLightGrayDottedLineColor
//{
//    return [UIColor colorWithIntRed:200 green:208 blue:213 alpha:1.0f];
//}
//
//+ (UIColor *)rs_currentProgressTint
//{
//    return [UIColor rs_successGreenColor];
//}
//
//+ (UIColor *)rs_currentTrackTint
//{
//    return [UIColor colorWithIntRed:228 green:242 blue:210 alpha:1.0];
//}
//
//+ (UIColor *)rs_lastBestProgressTint
//{
//    return [UIColor colorWithWhite:111.0f/255.0f alpha:1.0];
//}
//
//+ (UIColor *)rs_lastBestTrackTint
//{
//    return [UIColor colorWithWhite:220.0f/255.0f alpha:1.0];
//}
//
//+ (UIColor *)rs_downloadProgressTint
//{
//    return [UIColor rs_colorFromHex:0x3FA1D4];
//}
//
//+ (UIColor *)rs_downloadTrackTint
//{
//    return [UIColor whiteColor];
//}
//
//+ (UIColor *)rs_testButtonGrey
//{
//    return [UIColor colorWithWhite:71.0f/255.0f alpha:1.0];
//}

@end
