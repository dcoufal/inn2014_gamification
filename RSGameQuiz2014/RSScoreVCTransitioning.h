//
//  RSScoreVCTransitioning.h
//  RSGameQuiz2014
//
//  Created by David Coufal on 12/2/14.
//  Copyright (c) 2014 rosettastone. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface RSScoreVCTransitioning : NSObject<UIViewControllerAnimatedTransitioning>

@property (assign, nonatomic) BOOL presenting;

@end
